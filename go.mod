module github.com/AIF-Scoring-Engine-System/bill-service

go 1.13

require (
	github.com/golang/protobuf v1.5.2
	github.com/joho/godotenv v1.4.0
	github.com/satori/go.uuid v1.2.0
	google.golang.org/grpc v1.41.0
	gorm.io/driver/postgres v1.1.2
	gorm.io/gorm v1.21.15
)
